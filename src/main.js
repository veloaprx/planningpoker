import $ from 'jquery';

class Planningpoker{

    constructor(cardnumbers){ 
        this._cardnumbers = cardnumbers;
    }
    /** 
    Generate and append cards to container div.
    **/
    init(){
        this._cardnumbers.forEach((value) => {
            $("#container").append(`
                <div class="cards">
                    <div class="front">
                        <div class="popupfront">Tap card to play again!</div>
                        <div class="number-top">${value}</div>
                        <div class="number-center">${value}</div>
                        <div class="number-bottom">${value}</div>
                    </div>
                    <div class="back">
                        <img src="../assets/img/ChasLogoSvgWht.svg" class="cardlogo">
                        <div class="popupback">Tap card to reveal your estimation!</div>
                    </div>
                </div>`);
        });
        this.firstFlip();
    }

        // Pick a card
    firstFlip(){
        let that = this;
        let cards = $('.cards');        
        cards.on('click',function() {
                //turn of clickhandler for all cards
            cards.off('click');
            let card = $(this);    
                //change z-index, add flip-transition for selected card and fade-transition to its siblings.                      
            $("#bar, footer").addClass("fadetransition");
            card.css("z-index", "5").addClass("fliptransition").siblings().addClass("fadetransition"); 
                //Set property to offset from selected card to center of screen
            that.centerPos = that.getCenterPos(card);
                //do transform on selected card
            that.flip(card, "-180deg");
                //popup transition
            card.find(".popupback").addClass("popupanim");
            that.secondflip(card);
        });
    }

        //Show card (basically the same functionallaty as firstFlip())
    secondflip(card){
        card.on('click',()=> {
            card.off('click');
            card.find(".popupfront").addClass("popupanim");
            this.flip(card, "0deg");
            this.reset(card);
        });
    }

        //flip transitions.
    flip(card, degrees){ 
            //scroll to top and remove scrollbars
        $('html,body').animate({ scrollTop: 0 }, 500);     
        $('body').css("overflow","hidden");
            //add transform to card, translate, rotate and scale.
        card.css({"transform":"translate(" + this.centerPos.x + "px," + this.centerPos.y  + "px) rotateY("+degrees+") rotateX(5deg) scale(2)"}); 
    }
 
    //Get center of screen from card offset
    getCenterPos(card){
        let x = ($(window).width()/2) - (card.offset().left + card.outerWidth() / 2);
        let y = ($( window ).height()/2) - (card.offset().top + card.outerHeight() / 2);
        let centerPos = {
            x: x,
            y: y
        };
        return centerPos;
    }

    reset(card){		
        card.on("click", ()=>{  
            //reset css          
            $("#bar, footer").removeClass("fadetransition");          
            card.off("click").css({"transform":" none"}).siblings().css('z-index','0');
            $('body').css("overflow","visible");
            $(".cards").siblings().removeClass('fadetransition');            
            card.find(".popupback").removeClass("popupanim");
            card.find(".popupfront").removeClass("popupanim");
            
            //wait for transition to end before reinitiating
            setTimeout(()=> {
                card.removeClass("fliptransition");
                card.css("z-index","0");
                this.firstFlip();                
            }, 700);
        });
    }
}

var play = new Planningpoker([ 0, "&frac12;", 1, 2, 3, 5, 8, 13, 20, 40, 100, "?", "&infin;"]);
play.init();      


