var ExtractTextPlugin = require('extract-text-webpack-plugin');
const path = require('path');


module.exports = {
  entry: ['./src/main.js', './sass/style.scss'],
  output: {
    path: path.resolve(__dirname, 'build'),
    filename: 'main.bundle.js'
  },
    module: {
    loaders: [{ 
      test: /\.js$/, 
      exclude: /node_modules/, 
      loader: 'babel-loader'
    },
    {
    test: /\.scss$/,
     loader: ExtractTextPlugin.extract(['css-loader', 'sass-loader'])
      }
    ]
  },
   plugins: [
    new ExtractTextPlugin({
      filename: 'styles/style.css',
      allChunks: true,
    })
  ]
};