# Chas Planning Poker

## Installation

npm install

## Usage

npm start

http-server

## Teknikval

Jag valde att använda es6/es2015 för att det känns som det är standarden man bör använda just nu.
Jag provade att använda mig av es6 class dekleration för att prova på hur det kändes,
jag är osäkerpå om det är något jag skulle använda mig av igen, men det ser ganska rent ut även om
det kan kännas missvisande då det bara är "syntactic sugar".

För att underlätta DOM manupilering använde jag jQuery.
För att transpila till es5 använde jag babel och webpack för att bundla.
Jag använde sass för att underlätta stylningen men kunde säkert ha använt mig mer av det.

För versionshantering använde jag bitbucket och git.
Jag glömde att commita en hel del i början och därför blev det tyvärr en ganska stor commit till att börja med.
Jag hoppas det går bra ändå.

Om jag haft mer tid på mig så skulle jag nog prova att använda mig av något ramverk som react.
Kanske även göra någon multiplayer lösning så man kan se ens medspelares uppskattningar.
Eller så skulle jag ta mig ann att göra det i webgl med tex three.js.

## Credits

Andreas Ahlborg
0705667497
ahlborg_andreas@hotmail.com